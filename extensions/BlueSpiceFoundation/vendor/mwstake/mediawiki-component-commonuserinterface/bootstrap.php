<?php

$lessVars = \MWStake\MediaWiki\Component\CommonUserInterface\LessVars::getInstance();

// Provide the list of values
$lessVars->setVar( 'primary-bg', '' );
$lessVars->setVar( 'secondary-bg', '' );
$lessVars->setVar( 'neutral-bg', '' );
$lessVars->setVar( 'primary-fg', '' );
$lessVars->setVar( 'secondary-fg', '' );
$lessVars->setVar( 'neutral-fg', '' );
$lessVars->setVar( 'body-bg', '' );
$lessVars->setVar( 'content-bg', '' );
$lessVars->setVar( 'content-bg', '' );
$lessVars->setVar( 'content-width', '' );
