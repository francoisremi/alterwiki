<?php
# À chaque groupe d'utilisateur est
# associé un namespace privé.
# Par convention, les Groupes Locaux sont
# indexés de 3000 à 3999.
# Les groupes de travail nationaux sont
# indexés de 4000 à 4999.
# Un id est unique et l'on doit faire très attention
# si on supprime un groupe / namespace.

$wgNamespacePermissionLockdown[NS_MAIN]['read'] = [ 'sysop' ];

# ========= GRENOBLE =========
# Définition des constantes de namespace
define("NS_GRENOBLE", 3000); // This MUST be even.
define("NS_GRENOBLE_TALK", 3001); // This MUST be the following odd integer.

# Création des namespaces
$wgExtraNamespaces[NS_GRENOBLE] = "Grenoble";
$wgExtraNamespaces[NS_GRENOBLE_TALK] = "Grenoble_talk"; // Note underscores in the namespace name.

# Création du groupe
$wgGroupPermissions['Grenoble']['read'] = true;

# Droits dans le namespace restreints au groupe de Grenoble
$wgNamespacePermissionLockdown[NS_GRENOBLE]['read'] = [ 'Grenoble' ];
$wgNamespacePermissionLockdown[NS_GRENOBLE]['edit'] = [ 'Grenoble' ];

# Ajout d'autres membres
$wgAddGroups['Grenoble'] = [ 'Grenoble' ];

# Retrait des membres
$wgRemoveGroups['Grenoble'] = [ 'Grenoble' ];

# Visual Editor
$wgVisualEditorAvailableNamespaces[NS_GRENOBLE] = true;
?>
